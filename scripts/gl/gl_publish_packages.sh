#!/usr/bin/env bash

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

for file in .build/vscode-web-dist/* .build/vscode-reh/* ; do
	url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-web-ide-vscode-fork/$(cat FULL_VERSION)/$(basename "${file}")"
	curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${file}" "${url}"
done
