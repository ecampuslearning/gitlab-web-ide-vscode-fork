#!/usr/bin/env bash

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

npm config set scripts-prepend-node-path true
npm config set node_gyp

yarn monaco-compile-check
yarn valid-layers-check

# The next 3 tasks can be replaced by `gulp core-ci` if we are OK
# with running an extra `minify-vscode-reh-web` task
yarn gulp compile-build
yarn gulp minify-vscode
yarn gulp minify-vscode-reh

# The extension tasks are necessary
# Without them the server doesn't start because it's missing extensions
yarn gulp compile-extension-media
yarn gulp compile-extensions-build

declare -a os_and_archs=(
	"darwin-x64"
	"darwin-arm64"
	"win32-x64"
	"linux-x64"
	"linux-arm64"
)

for os_and_arch in "${os_and_archs[@]}"
do
	yarn gulp "vscode-reh-${os_and_arch}-min-ci"

	BUNDLE_NAME=vscode-reh-${os_and_arch}-$(cat FULL_VERSION)
	mkdir -p .build/vscode-reh
	tar -czvf ".build/vscode-reh/${BUNDLE_NAME}.tar.gz" -C "../vscode-reh-${os_and_arch}" .
done
